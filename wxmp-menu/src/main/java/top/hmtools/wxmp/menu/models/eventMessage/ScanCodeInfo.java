package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 扫描信息
 * @author Hybomyth
 *
 */
public class ScanCodeInfo {

	/**
	 * 扫描类型，一般是qrcode
	 */
	@XStreamAlias("ScanType")
	private String scanType;
	
	/**
	 * 扫描结果，即二维码对应的字符串信息
	 */
	@XStreamAlias("ScanResult")
	private String scanResult;

	public String getScanType() {
		return scanType;
	}

	public void setScanType(String scanType) {
		this.scanType = scanType;
	}

	public String getScanResult() {
		return scanResult;
	}

	public void setScanResult(String scanResult) {
		this.scanResult = scanResult;
	}

	@Override
	public String toString() {
		return "ScanCodeInfo [ScanType=" + scanType + ", ScanResult=" + scanResult + "]";
	}
	
	
}
