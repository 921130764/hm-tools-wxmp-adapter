package top.hmtools.javabean;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

import com.alibaba.fastjson.JSON;

import top.hmtools.javabean.models.AaaBean;
import top.hmtools.javabean.models.BbbBean;
import top.hmtools.javabean.models.CccBean;
import top.hmtools.wxmp.core.tools.Bean2MapTools;

public class BeanTest {

	@Test
	public void beanToMap() throws IOException{
		CccBean cccBean = new CccBean();
		cccBean.setCccAge(22);
		cccBean.setCccName("ccccccccccccc");
		
		BbbBean bbbBean = new BbbBean();
		bbbBean.setBbbName("bbbbbbbbbb");
		bbbBean.setFile(new File("c:\\bbb.txt"));
		bbbBean.setInputStream(IOUtils.toInputStream("aabbccddeeff", "utf-8"));
		bbbBean.setCccBean(cccBean);
		
		AaaBean aaaBean = new AaaBean();
		aaaBean.setAaaName("aaaaaaaaaaa");
		aaaBean.setBbbBean(bbbBean);
		aaaBean.setFile(new File("c:\\aaa.txt"));
		
		String jsonString = JSON.toJSONString(aaaBean);
		HashMap map = JSON.parseObject(jsonString, HashMap.class);
		System.out.println(map);
	}
	
	@Test
	public void copyProperties() throws IOException{
		CccBean cccBean = new CccBean();
		cccBean.setCccAge(22);
		cccBean.setCccName("ccccccccccccc");
		
		BbbBean bbbBean = new BbbBean();
		bbbBean.setBbbName("bbbbbbbbbb");
		bbbBean.setFile(new File("c:\\bbb.txt"));
		bbbBean.setInputStream(IOUtils.toInputStream("aabbccddeeff", "utf-8"));
		bbbBean.setCccBean(cccBean);
		
		AaaBean aaaBean = new AaaBean();
		aaaBean.setAaaName("aaaaaaaaaaa");
		aaaBean.setBbbBean(bbbBean);
		aaaBean.setFile(new File("c:\\aaa.txt"));
		
		HashMap<String, Object> map = new HashMap<>();
		BeanUtils.copyProperties(aaaBean, map);
		System.out.println(map);
		
	}
	
	
	@Test
	public void beanToMap2() throws IOException, IllegalAccessException{
		CccBean cccBean = new CccBean();
		cccBean.setCccAge(22);
		cccBean.setCccName("ccccccccccccc");
		
		BbbBean bbbBean = new BbbBean();
		bbbBean.setBbbName("bbbbbbbbbb");
		bbbBean.setFile(new File("c:\\bbb.txt"));
		bbbBean.setInputStream(IOUtils.toInputStream("aabbccddeeff", "utf-8"));
		bbbBean.setCccBean(cccBean);
		
		AaaBean aaaBean = new AaaBean();
		aaaBean.setAaaName("aaaaaaaaaaa");
		aaaBean.setBbbBean(bbbBean);
		aaaBean.setFile(new File("c:\\aaa.txt"));
		
		Map<String, Object> map = Bean2MapTools.bean2Map(aaaBean);
		System.out.println(map);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
