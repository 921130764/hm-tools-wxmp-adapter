package top.hmtools.wxmp.core.wxmpApiDemo;

import java.io.InputStream;
import java.util.HashMap;

import top.hmtools.javabean.models.BbbBean;
import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.annotation.WxmpParam;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.enums.HttpParamDataType;
import top.hmtools.wxmp.core.enums.HttpParamType;

@WxmpMapper
public interface WApiAAA {

	@WxmpApi(httpMethods = HttpMethods.GET, httpParamType = HttpParamType.FORM_DATA, uri = "/haha/dodo")
	public String doAAA(String p1, boolean p2, HashMap<String, String> p3);

	@WxmpApi(httpMethods = HttpMethods.GET, httpParamType = HttpParamType.FORM_DATA, uri = "/haha/bbb")
	public String doBBB(String p1, boolean p2, HashMap<String, String> p3);

	@WxmpApi(httpMethods = HttpMethods.POST, httpParamType = HttpParamType.FORM_DATA, uri = "/haha/ccc")
	public String doCCC(@WxmpParam(name = "pp1", httpParamDataType = HttpParamDataType.TEXT) String p1, 
			@WxmpParam(name = "pp2", httpParamDataType = HttpParamDataType.TEXT) boolean p2,
			@WxmpParam(name = "pp3", httpParamDataType = HttpParamDataType.JAVABEAN) HashMap<String, String> p3, 
			@WxmpParam(name = "pp4", httpParamDataType = HttpParamDataType.INPUTSTREAM) InputStream p4, 
			@WxmpParam(name = "pp5", httpParamDataType = HttpParamDataType.JAVABEAN) BbbBean bbb);
}
