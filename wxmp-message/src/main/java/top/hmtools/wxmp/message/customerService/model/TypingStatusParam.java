package top.hmtools.wxmp.message.customerService.model;

/**
 * Auto-generated: 2019-08-25 19:18:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class TypingStatusParam {

	/**
	 * 普通用户（openid）
	 */
	private String touser;
	
	/**
	 * "Typing"：对用户下发“正在输入"状态 "CancelTyping"：取消对用户的”正在输入"状态
	 */
	private String command;

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTouser() {
		return touser;
	}

	/**
	 * "Typing"：对用户下发“正在输入"状态 "CancelTyping"：取消对用户的”正在输入"状态
	 * @param command
	 */
	public void setCommand(String command) {
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

}