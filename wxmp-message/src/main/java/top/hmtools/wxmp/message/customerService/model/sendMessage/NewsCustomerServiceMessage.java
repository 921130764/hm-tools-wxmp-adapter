package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理--客服消息--发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。
 * @author HyboWork
 *
 */
public class NewsCustomerServiceMessage extends BaseSendMessageParam{

	private News news;

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	@Override
	public String toString() {
		return "NewsCustomerServiceMessage [news=" + news + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	
}
