#### -1. 开始前必读 ``
- 首页
- 更新日志
- 开发者规范
- 公众号接口权限说明
- 全局返回码说明
- 入门指引

#### 0. 开始开发
- 接入指南
- 接口域名说明
- 获取access_token
- 接口测试号申请
- 报警排查指引
- 接口在线调试
- 获取微信服务器IP地址
- 网络检测
- 常见问题

#### 1. 自定义菜单		`完成`
- 自定义菜单创建接口		top.hmtools.wxmp.menu.apis.IMenuApi.createMenu(MenuBean)
- 自定义菜单查询接口		top.hmtools.wxmp.menu.apis.IMenuApi.getMenu()
- 自定义菜单删除接口		top.hmtools.wxmp.menu.apis.IMenuApi.deleteAllMenu()
- 自定义菜单事件推送
  - 1 点击菜单拉取消息时的事件推送		top.hmtools.wxmp.menu.models.eventMessage.ClickEventMessage
  - 2 点击菜单跳转链接时的事件推送  		top.hmtools.wxmp.menu.models.eventMessage.ViewEventMessage
  - 3 scancode_push：扫码推事件的事件推送  	top.hmtools.wxmp.menu.models.eventMessage.ScancodePushEventMessage
  - 4 scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框的事件推送    top.hmtools.wxmp.menu.models.eventMessage.ScancodeWaitmsgEventMessage
  - 5 pic_sysphoto：弹出系统拍照发图的事件推送 		top.hmtools.wxmp.menu.models.eventMessage.PicSysPhotoEventMessage
  - 6 pic_photo_or_album：弹出拍照或者相册发图的事件推送  	top.hmtools.wxmp.menu.models.eventMessage.PicPhotoOrAlbumEventMessage
  - 7 pic_weixin：弹出微信相册发图器的事件推送  	top.hmtools.wxmp.menu.models.eventMessage.PicWeixinEventMessage
  - 8 location_select：弹出地理位置选择器的事件推送  	top.hmtools.wxmp.menu.models.eventMessage.LocationSelectEventMessage
  - 9 点击菜单跳转小程序的事件推送 		top.hmtools.wxmp.menu.models.eventMessage.ViewMiniprogramEventMessage
- 个性化菜单接口
  - 1 创建个性化菜单  		top.hmtools.wxmp.menu.apis.IConditionalMenuApi.addConditional(ConditionalMenuBean)
  - 2 删除个性化菜单  		top.hmtools.wxmp.menu.apis.IConditionalMenuApi.delConditional(ConditionalBean)
  - 3 测试个性化菜单匹配结果  	top.hmtools.wxmp.menu.apis.IConditionalMenuApi.tryMatch(TryMatchParamBean)
  - 4 查询个性化菜单  使用普通自定义菜单查询接口可以获取默认菜单和全部个性化菜单信息，请见自定义菜单查询接口的说明。 top.hmtools.wxmp.menu.apis.IMenuApi.getMenu()
  - 5 删除所有菜单  使用普通自定义菜单删除接口可以删除所有自定义菜单（包括默认菜单和全部个性化菜单），请见自定义菜单删除接口的说明。top.hmtools.wxmp.menu.MenuTools.deleteMenu()
- 获取自定义菜单配置接口		top.hmtools.wxmp.menu.models.simple.CurrentSelfMenuInfoBean

#### 2. 消息管理	`开发进行中（暂未测试）`
- 接收普通消息
    - 文本消息		top.hmtools.wxmp.message.ordinary.model.TextMessage
    - 图片消息		top.hmtools.wxmp.message.ordinary.model.ImageMessage
    - 语音消息		top.hmtools.wxmp.message.ordinary.model.VoiceMessage
    - 视频消息		top.hmtools.wxmp.message.ordinary.model.VideoMessage
    - 小视频消息		top.hmtools.wxmp.message.ordinary.model.ShortvideoMessage
    - 地理位置消息		top.hmtools.wxmp.message.ordinary.model.LocationMessage
    - 链接消息		top.hmtools.wxmp.message.ordinary.model.LinkMessage

- 接收事件推送
    - 1 关注/取消关注事件		top.hmtools.wxmp.message.eventPush.model.SubscribeEventMessage，top.hmtools.wxmp.message.eventPush.model.UnsubscribeEventMessage
    - 2 扫描带参数二维码事件		（未关注时，msgType、event 与 top.hmtools.wxmp.message.eventPush.model.SubscribeEventMessage同值）top.hmtools.wxmp.message.eventPush.model.QRSubscribeEventMessage，top.hmtools.wxmp.message.eventPush.model.QRScanEventMessage
    - 3 上报地理位置事件		top.hmtools.wxmp.message.eventPush.model.LocationEventMessage
    - 4 自定义菜单事件 即：5,6
      - 5 点击菜单拉取消息时的事件推送		top.hmtools.wxmp.message.eventPush.model.ClickMenuEventMessage
      - 6 点击菜单跳转链接时的事件推送		top.hmtools.wxmp.message.eventPush.model.ViewMenuEventMessage

- 被动回复用户消息
    - 1 回复文本消息		top.hmtools.wxmp.message.reply.model.ReplyTextMessage
    - 2 回复图片消息		top.hmtools.wxmp.message.reply.model.ReplyImageMessage
    - 3 回复语音消息		top.hmtools.wxmp.message.reply.model.ReplyVoiceMessage
    - 4 回复视频消息		top.hmtools.wxmp.message.reply.model.ReplyVideoMessage
    - 5 回复音乐消息		top.hmtools.wxmp.message.reply.model.ReplyMusicMessage
    - 6 回复图文消息		top.hmtools.wxmp.message.reply.model.ReplyNewsMessage

- 消息加解密说明		https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Message_encryption_and_decryption_instructions.html

- 客服消息
    - 1 客服帐号管理
    - 1.1 添加客服帐号		top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi.addKfAcount(KfAccountParam)
    - 1.2 修改客服帐号		top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi.updateKfAcount(KfAccountParam)
    - 1.3 删除客服帐号		top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi.deleteKfAcount(KfAccountParam)
    - 1.4 设置客服帐号的头像		top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi.uploadHeadImg(UploadHeadImgParam)
    - 1.5 获取所有客服账号		top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi.getKfList()
    - 1.6 接口的统一参数说明		https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#6
    - 2 客服接口-发消息		top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi.sendMessage(BaseSendMessageParam) ，top.hmtools.wxmp.message.customerService.model.sendMessage（消息数据结构所在包）
    		- 文本消息		top.hmtools.wxmp.message.customerService.model.sendMessage.TextCustomerServiceMessage
    		- 图片消息		top.hmtools.wxmp.message.customerService.model.sendMessage.ImageCustomerServiceMessage
    		- 语音消息		top.hmtools.wxmp.message.customerService.model.sendMessage.VoiceCustomerServiceMessage
    		- 视频消息		top.hmtools.wxmp.message.customerService.model.sendMessage.VideoCustomerServiceMessage
    		- 音乐消息		top.hmtools.wxmp.message.customerService.model.sendMessage.MusicCustomerServiceMessage
    		- 发送图文消息（点击跳转到外链）		top.hmtools.wxmp.message.customerService.model.sendMessage.NewsCustomerServiceMessage
    		- 发送图文消息（点击跳转到图文消息页面） top.hmtools.wxmp.message.customerService.model.sendMessage.MpnewsCustomerServiceMessage
    		- 发送菜单消息	top.hmtools.wxmp.message.customerService.model.sendMessage.MenuCustomerServiceMessage
    		- 发送卡券		top.hmtools.wxmp.message.customerService.model.sendMessage.WxcardCustomerServiceMessage
    		- 发送小程序卡片（要求小程序与公众号已关联）		top.hmtools.wxmp.message.customerService.model.sendMessage.MiniProgramCustomerServiceMessage
    - 3 客服接口-客服输入状态		top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi.typingStatus(TypingStatusParam)

- 群发接口和原创校验
    - 1 上传图文消息内的图片获取URL【订阅号与服务号认证后均可用】		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.uploadImage(UploadImageParam)
    - 2 上传图文消息素材【订阅号与服务号认证后均可用】		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.uploadNews(UploadNewsParam)
    - 3 根据标签进行群发【订阅号与服务号认证后均可用】		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.tagGroupMessageSend(BaseTagGroupSendParam)
    	- 图文消息（注意图文消息的media_id需要通过上述方法来得到）：		top.hmtools.wxmp.message.group.model.tagGroupSend.MpnewsTagGroupSendParam
    	- 文本：		top.hmtools.wxmp.message.group.model.tagGroupSend.TextTagGroupSendParam
    	- 语音/音频（注意此处media_id需通过素材管理->新增素材来得到）：		top.hmtools.wxmp.message.group.model.tagGroupSend.VoiceTagGroupSendParam
    	- 图片（注意此处media_id需通过素材管理->新增素材来得到）：		top.hmtools.wxmp.message.group.model.tagGroupSend.ImageTagGroupSendParam
    	- 视频		top.hmtools.wxmp.message.group.model.tagGroupSend.VideoTagGroupSendParam
    	- 卡券消息（注意图文消息的media_id需要通过上述方法来得到）：		top.hmtools.wxmp.message.group.model.tagGroupSend.WxcardTagGroupSendParam
    - 4 根据OpenID列表群发【订阅号不可用，服务号认证后可用】		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.openIdGroupMessageSend(BaseOpenIdGroupSendParam)
	     - 图文消息（注意图文消息的media_id需要通过上述方法来得到）：		top.hmtools.wxmp.message.group.model.openIdGroupSend.MpnewsOpenIdGroupSendParam
	     - 文本：	top.hmtools.wxmp.message.group.model.openIdGroupSend.TextOpenIdGroupSendParam
	     - 语音：	top.hmtools.wxmp.message.group.model.openIdGroupSend.VoiceOpenIdGroupSendParam
	     - 图片：	top.hmtools.wxmp.message.group.model.openIdGroupSend.ImageOpenIdGroupSendParam
	     - 视频：	top.hmtools.wxmp.message.group.model.openIdGroupSend.MpVideoOpenIdGroupSendParam
	     - 卡券：	top.hmtools.wxmp.message.group.model.openIdGroupSend.WxcardOpenIdGroupSendParam
    - 5 删除群发【订阅号与服务号认证后均可用】		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.deleteGroupMessageSended(DeleteParam)
    - 6 预览接口【订阅号与服务号认证后均可用】		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.previewGroupMessageSend(BasePreviewParam)
    	- 图文消息（其中media_id与根据分组群发中的media_id相同）：	top.hmtools.wxmp.message.group.model.preview.MpnewsPreviewParam
    	- 文本：		top.hmtools.wxmp.message.group.model.preview.TextPreviewParam
    	- 语音（其中media_id与根据分组群发中的media_id相同）：		top.hmtools.wxmp.message.group.model.preview.VoicePreviewParam
    	- 图片（其中media_id与根据分组群发中的media_id相同）：		top.hmtools.wxmp.message.group.model.preview.ImagePreviewParam
    	- 视频（其中media_id与根据分组群发中的media_id相同）：		top.hmtools.wxmp.message.group.model.preview.MpvedioPreviewParam
    	- 卡券：		top.hmtools.wxmp.message.group.model.preview.WxcardPreviewParam
    - 7 查询群发消息发送状态【订阅号与服务号认证后均可用】		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.getGroupMessageSendStatus(GroupMessageSendStatusParam)
    - 8 事件推送群发结果		top.hmtools.wxmp.message.group.model.event.GroupMessageSendEvent
    - 9 使用 clientmsgid 参数，避免重复推送		https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#8
    - 10 控制群发速度		如下
    	- 获取群发速度		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.getGroupMessageSendSpeed()
    	- 设置群发速度		top.hmtools.wxmp.message.group.apis.IGroupMessageApi.setGroupMessageSendSpeed(SpeedParam)

- 模板消息接口
    - 1 设置所属行业			top.hmtools.wxmp.message.template.apis.ITemplateApi.setIndustry(SetIndustryParam)
    - 2 获取设置的行业信息		top.hmtools.wxmp.message.template.apis.ITemplateApi.getIndustry()
    - 3 获得模板ID		top.hmtools.wxmp.message.template.apis.ITemplateApi.getTemplateId(TemplateIdParam)
    - 4 获取模板列表		top.hmtools.wxmp.message.template.apis.ITemplateApi.getAllPrivateTemplate()
    - 5 删除模板		top.hmtools.wxmp.message.template.apis.ITemplateApi.delPrivateTemplate(DelTemplateParam)
    - 6 发送模板消息		top.hmtools.wxmp.message.template.apis.ITemplateApi.sendTemplateMessage(SendTemplateMessageParam)
    - 7 事件推送		top.hmtools.wxmp.message.template.model.event.TemplateMessageSendedEvent

- 一次性订阅消息

- 模板消息运营规范

- 接口调用频次限制说明

- 获取公众号的自动回复规则

#### 3. 微信网页开发	`完成（可能需要改良）`
- iOS WKWebview 网页开发适配指南		网页开发适配指南  略
- 微信网页授权		top.hmtools.wxmp.webpage.authorize.OAuth2Tools
- 微信网页开发样式库		 http://weui.io/
- 微信JS-SDK说明文档		top.hmtools.wxmp.webpage.jsSdk.JsSdkTools
- 微信web开发者工具		略

#### 4. 素材管理	`完成`
- 新增临时素材		top.hmtools.wxmp.material.apis.ITemporaryApi.uploadMedia(UploadParam)
- 获取临时素材		top.hmtools.wxmp.material.apis.ITemporaryApi.getImage(MediaParam) ， top.hmtools.wxmp.material.apis.ITemporaryApi.getVideo(MediaParam)
- 新增永久素材		top.hmtools.wxmp.material.apis.IForeverApi.addNews(NewsBean)，top.hmtools.wxmp.material.apis.IForeverApi.uploadImageForNews(UploadParam) ， top.hmtools.wxmp.material.apis.IForeverApi.addMaterial(UploadParam)
- 获取永久素材		top.hmtools.wxmp.material.apis.IForeverApi.getMaterialImage(MediaParam)
- 删除永久素材		top.hmtools.wxmp.material.apis.IForeverApi.delMaterial(MediaParam)
- 修改永久图文素材		top.hmtools.wxmp.material.apis.IForeverApi.updateNews(NewsBeanForUpdate) `未测试通`
- 获取素材总数		top.hmtools.wxmp.material.apis.IForeverApi.getMaterialCount()
- 获取素材列表		top.hmtools.wxmp.material.apis.IForeverApi.getBatchgetMaterial(BatchgetMaterialParam)

#### 5. 图文消息留言管理	`完成（但暂未测试）`
- 图文消息留言管理接口
  - 1.1 新增永久素材（原接口有所改动）	4. 素材管理
  - 1.2 获取永久素材（原接口有所改动）	4. 素材管理
  - 1.3 修改永久图文素材（原接口有所改动）	4. 素材管理
  - 1.4 获取素材列表（原接口有所改动）	4. 素材管理
  - 2.1 打开已群发文章评论（新增接口）		top.hmtools.wxmp.comment.apis.ICommentApi.openComment(OpenCommentParam)
  - 2.2 关闭已群发文章评论（新增接口）		top.hmtools.wxmp.comment.apis.ICommentApi.closeComment(OpenCommentParam)
  - 2.3 查看指定文章的评论数据（新增接口）		top.hmtools.wxmp.comment.apis.ICommentApi.listComment(ListCommentParam)
  - 2.4 将评论标记精选（新增接口）		top.hmtools.wxmp.comment.apis.ICommentApi.markelectComment(UserCommentParam)
  - 2.5 将评论取消精选		top.hmtools.wxmp.comment.apis.ICommentApi.unmarkelectComment(UserCommentParam)
  - 2.6 删除评论（新增接口）		top.hmtools.wxmp.comment.apis.ICommentApi.deleteComment(UserCommentParam)
  - 2.7 回复评论（新增接口）		top.hmtools.wxmp.comment.apis.ICommentApi.replyComment(ReplyCommentParam)
  - 2.8 删除回复（新增接口）		top.hmtools.wxmp.comment.apis.ICommentApi.deleteReplyComment(UserCommentParam)


#### 6. 用户管理	`完成`
- 用户标签管理
 - 1. 创建标签		top.hmtools.wxmp.user.apis.ITagsApi.create(TagWapperParam)
 - 2. 获取公众号已创建的标签		top.hmtools.wxmp.user.apis.ITagsApi.get()
 - 3. 编辑标签		top.hmtools.wxmp.user.apis.ITagsApi.update(TagWapperParam)
 - 4. 删除标签		top.hmtools.wxmp.user.apis.ITagsApi.delete(TagWapperParam)
 - 5. 获取标签下粉丝列表		top.hmtools.wxmp.user.apis.ITagsApi.getFunsOfTag(TagFunsParam)
 - 1. 批量为用户打标签		top.hmtools.wxmp.user.apis.ITagsApi.batchTagging(BatchTagParam)
 - 2. 批量为用户取消标签		top.hmtools.wxmp.user.apis.ITagsApi.batchUntagging(BatchTagParam)
 - 3. 获取用户身上的标签列表		top.hmtools.wxmp.user.apis.ITagsApi.getTaglistByOpenid(TagListParam)

- 设置用户备注名		top.hmtools.wxmp.user.apis.IRemarkApi.updateRemark(RemarkParam)
- 获取用户基本信息(UnionID机制)
 - 获取用户基本信息（包括UnionID机制）	top.hmtools.wxmp.user.apis.IUnionIDApi.getUserInfo(UserInfoParam)
 - 批量获取用户基本信息		top.hmtools.wxmp.user.apis.IUnionIDApi.getBatchUserInfo(BatchUserInfoParam)

- 获取用户列表		top.hmtools.wxmp.user.apis.IUserListApi.getUserList(UserListParam)
- 获取用户地理位置		top.hmtools.wxmp.user.model.eventMessage.LocationMessage
- 黑名单管理
 - 1. 获取公众号的黑名单列表		top.hmtools.wxmp.user.apis.IBlackListApi.getBlackList(BlackListParam)
 - 2. 拉黑用户		top.hmtools.wxmp.user.apis.IBlackListApi.batchBlackList(BatchBlackListParam)
 - 3. 取消拉黑用户		top.hmtools.wxmp.user.apis.IBlackListApi.batchUnblackList(BatchBlackListParam)

#### 7. 帐号管理		`完成`
- 生成带参数的二维码		top.hmtools.wxmp.account.apis.IQrcodeApis.create(QRCodeParam)，top.hmtools.wxmp.account.apis.IQrcodeApis.showQrCode(TicketParam)
- 长链接转短链接接口		top.hmtools.wxmp.account.apis.IShortUrlApis.getShorUrl(ShorturlParam)
- 微信认证事件推送
 - 1 资质认证成功（此时立即获得接口权限）	top.hmtools.wxmp.account.models.eventMessage.QualificationVerifySuccess
 - 2 资质认证失败		top.hmtools.wxmp.account.models.eventMessage.QualificationVerifyFail
 - 3 名称认证成功（即命名成功）		top.hmtools.wxmp.account.models.eventMessage.NamingVerifySuccess
 - 4 名称认证失败（这时虽然客户端不打勾，但仍有接口权限		top.hmtools.wxmp.account.models.eventMessage.NamingVerifyFail
 - 5 年审通知		top.hmtools.wxmp.account.models.eventMessage.AnnualRenew
 - 6 认证过期失效通知		top.hmtools.wxmp.account.models.eventMessage.VerifyExpired

#### 8. 数据统计
- 用户分析数据接口
- 图文分析数据接口
- 消息分析数据接口
- 接口分析数据接口

#### 9. 微信卡券
- 微信卡券接口
- 更新日志
- 创建卡券
- 投放卡券
- 核销卡券
- 管理卡券
- 卡券事件推送
- 卡券-小程序打通
- 微信礼品卡
- 会员卡专区
  - 会员卡玩法介绍
  - 创建会员卡
  - 管理会员卡
- 社交立减金专区
  - 社交立减金升级调整通知
- 朋友的券
  - 朋友的券说明
  - 创建朋友的券
  - 配置库存、充值券点
  - 投放朋友的券
  - 朋友的券核销
  - 朋友的券管理接口
- 特殊票券
- 卡券错误码
- 第三方开发者模式

#### 10. 微信门店
- 微信门店接口
- 微信门店小程序接口文档

#### 11. 微信小店
- 微信小店接口

#### 12. 智能接口
- 语义理解
- AI开放接口
- OCR识别

#### 13. 微信设备功能
- 设备功能介绍
- 如何新增产品型号

#### 14. 新版客服功能	`开发进行中`
- 将消息转发到客服
- 客服管理
- 会话控制
- 获取聊天记录

#### 15. 微信摇一摇周边
- 申请开通摇一摇周边
  - ​申请开通功能
  - 查询审核状态
- 设备管理
  - 申请设备ID
  - 查询设备ID申请审核状态
  - 编辑设备信息
  - 配置设备与门店的关联关系
  - 配置设备与其他公众账号门店的关联关系
  - 查询设备列表
- 页面管理
  - 页面管理
  - 编辑页面信息
  - ​ 查询页面列表
  - 删除页面
- ​上传图片素材
- 管理设备与页面的关联关系
  - 配置设备与页面的关联关系
  - 查询设备与页面的关联关系
- 数据统计
  - 以设备为维度的数据统计接口
  - 批量查询设备统计数据接口
  - 以页面为维度的数据统计接口
  - 批量查询页面统计数据接口
- 摇一摇关注JSAPI
- 摇一摇事件通知
- Html5页面获取设备信息
  - 新增分组
  - 编辑分组信息
  - 删除分组
  - 查询分组列表
  - 查询分组详情
  - 添加设备到分组
  - 从分组中移除设备
  - ​H5页面获取设备信息 JS API
- 获取设备及用户信息
- 摇一摇红包
  - 摇一摇红包说明
  - 红包预下单接口
  - 创建红包活动
  - 录入红包信息
  - 设置红包活动抽奖开关
  - 红包JSAPI
  - 红包绑定用户事件通知
  - 红包查询接口
- 摇一摇周边错误码

#### 16. 微信连Wi-Fi
- 微信连Wi-Fi开发者指引
- Wi-Fi硬件鉴权协议接口说明
- Wi-Fi软件服务管理接口说明
- 开通微信连Wi-Fi插件
- 连Wi-Fi-小程序打通接口文档
- Wi-Fi门店管理
  - 获取Wi-Fi门店列表
  - 查询门店Wi-Fi信息
  - 修改门店网络信息
  - 清空门店网络及设备
- Wi-Fi设备管理
  - 添加密码型设备
  - 添加portal型设备
  - 查询设备
  - 删除设备
- 配置连网方式
  - 获取物料二维码
- 商家主页管理
  - 设置商家主页
  - 查询商家主页
  - 设置微信首页欢迎语
  - 设置连网完成页
- Wi-Fi数据统计
- 连网后下发消息
- 卡券投放
  - 设置门店卡券投放信息
  - 查询门店卡券投放信息
- Wi-Fi错误码
- 连网过程常见错误码

#### 17. 微信扫一扫
- 扫一扫接入指南
- 商品创建
- 商品发布
- 商品管理
- 扫一扫事件推送
- 一物一码专区
- 错误码
- 在线帮助工具
- 扫一扫常见问题

#### 18. 微信发票
- 微信电子发票
  - 业务介绍
  - 选择接入模式
  - “商户+开票平台”模式说明
  - “自建平台”模式说明
  - 微信“开票接入能力”模式说明
  - 报销场景说明
  - 商户接口列表
  - 开票平台接口列表
  - 报销方接口列表
- 微信极速开发票
  - 微信极速开票方案介绍
  - 接入申请
  - 角色及流程图
  - 接口说明
  - 二维码立牌设计
- 电子发票自助打印
  - 业务介绍
  - 接口文档
  - 设计素材
- 非税票据
  - 业务介绍
  - 接入财政电子票据
  - 接口列表

#### 19. 其他文档
- 微信公众平台 · 小程序文档
- 微信开放平台文档

#### 20. 微信商品
- testczc
  - 微信非税缴费

#### 21. 微信非税缴费
- 微信非税接口文档
- 微信非税接入指引
- 微信非税车主平台接口文档

#### 22. 分布式支持